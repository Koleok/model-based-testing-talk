# Model based testing talk

### notes:

- There is **no such thing** as a self-verifying system
- There **is such a thing** as a more robust method of verifying a system
- Trickiest part is controlling timing of network requests
- System under test does not need to the implemented as a state machine, or in a certain JavaScript framework, or even be a user interface
- The same test model can be reused in different test environments by patching in controls and assertions

### excerpts:

- [Tweet](https://twitter.com/jeanqasaur/status/1389645922183696384) from 🎄 Jean Yang 🎄
  > When we talk about "developer experience," we really need to separate dev tools into two categories: ones that simplify things away and ones that help developers engage with complexity. DX needs are different for simplification tools vs complexity-embracing tools!
- from [The symbolic boundary of statecharts; hidden between the visual formalism and executable schematic](https://www.stateml.org/garden/symbolic-boundry.html)
  > It is this boundary that we believe will help make StateML agnostic to any given language/environment, while furthermore enabling an author to more quickly iterate and simulate a statechart since they can push off writing implementation details until they feel ready too!
- from [Developer Experience: Stuck Between Abstraction and a Hard Place?](https://www.akitasoftware.com/blog-posts/developer-experience-stuck-between-abstraction-and-a-hard-place)
  > **When an abstraction tool has complexity-embracing parts, everybody often looks the other way**.
  >
  > We have a lot of language for things being “easy,” “one-click,” and “like magic.” In part because this is what’s associated with good developer experience and products that make money, tool creators and users alike often ignore the parts of products that are not like this. The non-automatable parts of tools become the elephants in the room, weighing down developer experience. Problems can only get fixed if you acknowledge them!‍
