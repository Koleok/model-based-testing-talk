type Resolver = (data?: any) => any

/**
 * An extension of the standard Promise class that allows for a resolve
 * function to be called on demand.
 */
export type Deferral = Promise<any> & {
  /**
   * Resolves the deferred promise manually.
   */
  resolve: Resolver
}

/**
 * Create a new Deferral with optional data to resolve with.
 *
 * @param data Optional data to resolve the deferral with
 * @returns Deferral
 */
export const Deferral = (data?: any): Deferral => {
  let resolve: Resolver = () => {}

  const promise = new Promise((_resolve) => {
    resolve = (x?: any) => _resolve(x || data)
  })

  return Object.assign(promise, { resolve })
}

/**
 * Creates a stateful set of deferral objects which can be resolved
 * on demand in a `last in first out` order, or all at once.
 *
 * @returns DefferalSet object
 */
export const DeferralSet = () => {
  const deferrals: Deferral[] = []

  return {
    /**
     * A set of defferals that can be resolved on demand in a
     * `last in first out order`
     */
    deferrals,

    /**
     * Adds a deferral to the set
     *
     * @param data Optional data to resolve the deferral with
     * @returns
     */
    add(data?: any) {
      const deferral = Deferral(data)
      deferrals.push(deferral)
      return deferral
    },

    /**
     * Resolves the last deferral added to the set with optional data
     *
     * @param data Optional data to resolve the deferral with
     */
    resolveLast(data?: any) {
      deferrals.pop()?.resolve(data)
    },

    /**
     * Resolves all deferral objects in the set in parallel
     */
    resolveAll() {
      deferrals.forEach((deferral) => deferral.resolve())
    },
  }
}

export type DeferralSet = ReturnType<typeof DeferralSet>
