import { createModel as createTestModel } from '@xstate/test'

import { createModel } from 'xstate/lib/model'
interface TestContext {
  cy: typeof cy
}

const model = createModel(undefined, {
  events: {
    FOCUS_INPUT: () => ({}),
    TYPE_TODO: () => ({}),
    SUBMIT_TODO: () => ({}),
    DELETE_TODO: () => ({}),

    SELECT_ADDED_TODO: () => ({}),
    SELECT_EXISTING_TODO: () => ({}),
    CHANGE_ADDED_TODO: () => ({}),
    CHANGE_EXISTING_TODO: () => ({}),

    MARK_ADDED_COMPLETE: () => ({}),
    MARK_EXISTING_COMPLETE: () => ({}),

    DELETE_ALL: () => ({}),
    CLEAR_COMPLETED: () => ({}),

    CLEAR_FILTER: () => ({}),
    FILTER_COMPLETE: () => ({}),
    FILTER_ACTIVE: () => ({}),
    FILTER_COMPLETE_BY_URL: () => ({}),
    FILTER_ACTIVE_BY_URL: () => ({}),
  },
})

const addedTodo = 'Learn about model based testing'

const machine = model.createMachine({
  id: 'Cypress demo app',
  initial: 'default',
  on: {
    FOCUS_INPUT: 'readyToComposeTodo',
  },
  states: {
    default: {
      id: 'default',
      meta: {
        description: 'Two new todos are displayed by default',
        test({ cy }: TestContext) {
          cy.get('.todo-list li').should('have.length', 2)
          cy.get('.todo-list li')
            .first()
            .should('have.text', 'Pay electric bill')
          cy.get('.todo-list li').last().should('have.text', 'Walk the dog')
        },
      },
    },

    empty: {
      id: 'empty',
      meta: {
        description: 'All todos have been deleted',
        test({ cy }: TestContext) {
          cy.get('.todo-list li').should('have.length', 0)
        },
      },
    },

    readyToComposeTodo: {
      meta: {
        description: 'New todo input field is focused',
        test({ cy }: TestContext) {
          cy.get('[data-test=new-todo]').should('have.focus')
        },
      },
      on: {
        TYPE_TODO: 'composingTodo',
      },
    },

    composingTodo: {
      meta: {
        description: 'New todo is being composed',
        test({ cy }: TestContext) {
          cy.get('[data-test=new-todo]').should('have.value', addedTodo)
        },
      },
      on: {
        SUBMIT_TODO: 'userCreatedTodoExists',
      },
    },

    userCreatedTodoExists: {
      initial: 'idle',
      meta: {
        test({ cy }: TestContext) {
          cy.contains(addedTodo).should('exist')
          cy.get('.todo-list li').last().should('contain.text', addedTodo)
        },
      },

      states: {
        idle: {
          description: 'User-created todo exists',
          meta: {
            test({ cy }: TestContext) {
              cy.contains(addedTodo).should('exist')
              cy.get('.todo-list li').last().should('have.text', addedTodo)
            },
          },
          on: {
            DELETE_TODO: '#userCreatedTodoDoesNotExist',
            SELECT_ADDED_TODO: 'editing',
            MARK_ADDED_COMPLETE: 'markedComplete',
          },
        },

        editing: {
          meta: {
            description: 'New todo is being edited',
            test({ cy }: TestContext) {
              cy.contains(addedTodo)
                .parent()
                .parent()
                .should('have.class', 'editing')
              cy.contains(addedTodo).get('input').should('have.focus')
            },
          },
          on: {
            DELETE_TODO: '#userCreatedTodoDoesNotExist',
            CHANGE_ADDED_TODO: 'edited',
            MARK_ADDED_COMPLETE: 'markedComplete',
          },
        },

        edited: {
          meta: {
            description: 'New todo has been edited',
            test({ cy }: TestContext) {
              cy.contains(addedTodo).should('include.text', ' edited')
            },
          },
          on: {
            DELETE_TODO: '#userCreatedTodoDoesNotExist',
            MARK_ADDED_COMPLETE: 'markedComplete',
          },
        },

        markedComplete: {
          meta: {
            description: 'New todo is marked complete',
            test({ cy }: TestContext) {
              cy.get('.todo-list li.completed')
                .should('have.length', 1)
                .should('contain.text', addedTodo)
              cy.get('.todo-button.clear-completed').should('be.visible')
            },
          },
          on: {
            CLEAR_COMPLETED: '#userCreatedTodoDoesNotExist',
          },
        },
      },
    },

    userCreatedTodoDoesNotExist: {
      id: 'userCreatedTodoDoesNotExist',
      initial: 'allShown',
      meta: {
        description: 'User-created todo does not exist',
        test({ cy }: TestContext) {
          cy.contains(addedTodo).should('not.exist')
        },
      },
      on: {
        DELETE_ALL: '#empty',
      },
      states: {
        allShown: {
          meta: {
            description: 'Both completed and active todos are shown',
            test({ cy }: TestContext) {
              cy.get('.filters li:nth-of-type(1) a').should(
                'have.class',
                'selected'
              )
            },
          },
          on: {
            DELETE_ALL: '#empty',
            SELECT_EXISTING_TODO: 'editing',
            FILTER_COMPLETE: 'onlyCompleteShown',
            FILTER_ACTIVE: 'onlyActiveShown',
            FILTER_COMPLETE_BY_URL: 'onlyCompleteShown',
            FILTER_ACTIVE_BY_URL: 'onlyActiveShown',
          },
        },

        editing: {
          meta: {
            description: 'Existing first todo is being edited',
            test({ cy }: TestContext) {
              cy.get('.todo-list li').first().should('have.class', 'editing')
              cy.get('.todo-list li').first().get('input').should('have.focus')
            },
          },
          on: {
            DELETE_ALL: '#empty',
            CHANGE_EXISTING_TODO: 'edited',
          },
        },

        edited: {
          meta: {
            description: 'Existing todo has been edited',
            test({ cy }: TestContext) {
              cy.get('.todo-list li').first().should('include.text', ' edited')
            },
          },
          on: {
            DELETE_ALL: '#empty',
            MARK_EXISTING_COMPLETE: 'markedComplete',
          },
        },

        markedComplete: {
          meta: {
            description: 'Existing todo is marked complete',
            test({ cy }: TestContext) {
              cy.get('.todo-list li.completed')
                .should('have.length', 1)
                .should('include.text', 'Pay electric bill')
              cy.get('.todo-button.clear-completed').should('be.visible')
            },
          },
          on: {
            SELECT_EXISTING_TODO: 'editing',
            CLEAR_COMPLETED: 'completedTodosCleared',
            FILTER_COMPLETE: 'onlyCompleteShown',
            FILTER_ACTIVE: 'onlyActiveShown',
            FILTER_COMPLETE_BY_URL: 'onlyCompleteShown',
            FILTER_ACTIVE_BY_URL: 'onlyActiveShown',
          },
        },

        onlyCompleteShown: {
          meta: {
            description: 'Only completed todos are shown',
            test({ cy }: TestContext) {
              cy.get('.todo-list li').should((items) => {
                if (items.length) {
                  expect(items).to.have.class('completed')
                }
              })
            },
          },
          on: {
            CLEAR_FILTER: 'allShown',
          },
        },

        onlyActiveShown: {
          meta: {
            description: 'Only active todos are shown',
            test({ cy }: TestContext) {
              cy.get('.todo-list li').should((items) => {
                if (items.length) {
                  expect(items).to.not.have.class('completed')
                }
              })
            },
          },
          on: {
            CLEAR_FILTER: 'allShown',
          },
        },

        completedTodosCleared: {
          meta: {
            description: 'All completed todos have been cleared',
            test({ cy }: TestContext) {
              cy.get('.todo-list li.completed').should('have.length', 0)
              cy.get('.todo-button.clear-completed').should('not.be.visible')
            },
          },
        },
      },
    },
  },
})

const testModel = createTestModel<TestContext>(machine).withEvents({
  FOCUS_INPUT: {
    exec({ cy }: TestContext) {
      cy.get('[data-test=new-todo]').focus()
    },
  },
  TYPE_TODO: {
    exec({ cy }: TestContext) {
      cy.get('[data-test=new-todo]').type(addedTodo)
    },
  },
  SUBMIT_TODO: {
    exec({ cy }: TestContext) {
      cy.get('[data-test=new-todo]').type('{enter}')
    },
  },
  DELETE_TODO: {
    exec({ cy }: TestContext) {
      cy.contains(addedTodo).siblings('.destroy').click({ force: true })
    },
  },
  SELECT_ADDED_TODO: {
    exec({ cy }: TestContext) {
      cy.contains(addedTodo).dblclick()
    },
  },
  CHANGE_ADDED_TODO: {
    exec({ cy }: TestContext) {
      cy.contains(addedTodo).parent().siblings('input').type(' edited{enter}')
    },
  },
  SELECT_EXISTING_TODO: {
    exec({ cy }: TestContext) {
      cy.get('.todo-list li').first().dblclick()
    },
  },
  CHANGE_EXISTING_TODO: {
    exec({ cy }: TestContext) {
      cy.get('.todo-list li input.edit').first().type(' edited{enter}')
    },
  },
  MARK_ADDED_COMPLETE: {
    exec({ cy }: TestContext) {
      cy.contains(addedTodo).siblings('input').check({ force: true })
    },
  },
  MARK_EXISTING_COMPLETE: {
    exec({ cy }: TestContext) {
      cy.get('.todo-list li .toggle').first().check({ force: true })
    },
  },
  CLEAR_FILTER: {
    exec({ cy }: TestContext) {
      cy.get('.filters li:nth-of-type(1) a').click()
    },
  },
  FILTER_ACTIVE: {
    exec({ cy }: TestContext) {
      cy.get('.filters li:nth-of-type(2) a').click()
    },
  },
  FILTER_COMPLETE: {
    exec({ cy }: TestContext) {
      cy.get('.filters li:nth-of-type(3) a').click()
    },
  },
  FILTER_ACTIVE_BY_URL: {
    exec({ cy }: TestContext) {
      cy.visit('https://example.cypress.io/todo#/active')
    },
  },
  FILTER_COMPLETE_BY_URL: {
    exec({ cy }: TestContext) {
      cy.visit('https://example.cypress.io/todo#/completed')
    },
  },
  DELETE_ALL: {
    exec({ cy }: TestContext) {
      cy.get('.filters li:nth-of-type(1) a').click()
      cy.get('button.destroy').each((item) =>
        cy.wrap(item).click({ force: true })
      )
    },
  },
  CLEAR_COMPLETED: {
    exec({ cy }: TestContext) {
      cy.get('.todo-button.clear-completed').click()
    },
  },
})

describe('Cypress Todo App', () => {
  const testPlans = testModel.getSimplePathPlans()
  beforeEach(() => {
    // Reset to default state manually
    localStorage.setItem(
      'todos-vanillajs',
      JSON.stringify([
        { title: 'Pay electric bill', completed: false, id: 1640289892007 },
        { title: 'Walk the dog', completed: false, id: 1640289892007 },
      ])
    )
  })

  testPlans.forEach((plan) => {
    describe(plan.description, () => {
      plan.paths.forEach((path) => {
        it(path.description, () => {
          cy.visit('https://example.cypress.io/todo').then(() =>
            path.test({ cy })
          )
        })
      })
    })
  })

  // it('should have full coverage', () => {
  //   return testModel.testCoverage()
  // })
})

// describe('example to-do app', () => {
//   beforeEach(() => {
//     cy.visit('https://example.cypress.io/todo')
//   })

//   it('displays two todo items by default', () => {
//     cy.get('.todo-list li').should('have.length', 2)
//     cy.get('.todo-list li').first().should('have.text', 'Pay electric bill')
//     cy.get('.todo-list li').last().should('have.text', 'Walk the dog')
//   })

//   it('can add new todo items', () => {
//     const newItem = 'Feed the cat'
//     cy.get('[data-test=new-todo]').type(`${newItem}{enter}`)
//     cy.get('.todo-list li')
//       .should('have.length', 3)
//       .last()
//       .should('have.text', newItem)
//   })

//   it('can check off an item as completed', () => {
//     cy.contains('Pay electric bill')
//       .parent()
//       .find('input[type=checkbox]')
//       .check()
//     cy.contains('Pay electric bill')
//       .parents('li')
//       .should('have.class', 'completed')
//   })

//   context('with a checked task', () => {
//     beforeEach(() => {
//       cy.contains('Pay electric bill')
//         .parent()
//         .find('input[type=checkbox]')
//         .check()
//     })

//     it('can filter for uncompleted tasks', () => {
//       cy.contains('Active').click()
//       cy.get('.todo-list li')
//         .should('have.length', 1)
//         .first()
//         .should('have.text', 'Walk the dog')
//       cy.contains('Pay electric bill').should('not.exist')
//     })

//     it('can filter for completed tasks', () => {
//       cy.contains('Completed').click()
//       cy.get('.todo-list li')
//         .should('have.length', 1)
//         .first()
//         .should('have.text', 'Pay electric bill')

//       cy.contains('Walk the dog').should('not.exist')
//     })

//     it('can delete all completed tasks', () => {
//       cy.contains('Clear completed').click()
//       cy.get('.todo-list li')
//         .should('have.length', 1)
//         .should('not.have.text', 'Pay electric bill')
//       cy.contains('Clear completed').should('not.exist')
//     })
//   })
// })
